{-
    A = LU,   AX = Y

        L           U        X         Y
    [1  0  0]   [d  e  f]   [x1]      [y1]
    [a  1  0] . [0  g  h] . [x2]   =  [y2] 
    [b  c  1]   [0  0  i]   [x3]      [y3]
                <------[z1]----->
                       [z2]
                       [z3]

        [z1]
    Z = [z2]
        [z3]


	LZ = Y
    
	AX = Y ,   LUX = Y   , UX = Z ,  LZ = Y  

-}

import Data.List (intercalate)
import Text.Printf (printf)


matrix :: Fractional a  => [[a]]
matrix = [[2, 2, 2],
          [4, 7, 7],
          [6, 18, 22]
          ] 

matrix_Y :: Fractional a => [a]
matrix_Y = [12, 24, 12]



print_matrix :: [[Double]] -> Int -> IO()
print_matrix matrix row = do
    if length matrix == row 
        then return()
        else do
            let formatted_elements = map (printf "%-11.4f") (matrix !! row)
                formatted_string = intercalate " " formatted_elements

            putStrLn formatted_string   
            print_matrix matrix (row+1)




-- return L and U 
get_matrix_L_and_U :: Fractional a => [[a]] -> [[[a]]]
get_matrix_L_and_U matrix =   
    let a11 = matrix !! 0 !! 0
        a21 = matrix !! 1 !! 0
        a31 = matrix !! 2 !! 0
        row2 = zipWith (-) (matrix !! 1)  (map (*(a21/a11)) (matrix !! 0) )
        row3 = zipWith (-) (matrix !! 2)  (map (*(a31/a11)) (matrix !! 0) )
        a22_ = row2 !! 1
        a32_ = row3 !! 1
        row3_ = zipWith (-) row3  (map (*(a32_/a22_)) row2 )
        matrix_L = [[1, 0, 0], [a21/a11, 1, 0], [a31/a11, a32_/a22_, 1]]
        matrix_U = [ matrix !! 0, row2, row3_ ]
        

    in [matrix_L, matrix_U]
 

get_matrix_Z :: Fractional a => [[a]] -> [a] -> [a]
get_matrix_Z lower_tmatrix matrix_Y = 
    let z1 = matrix_Y !! 0
        z2 = matrix_Y !! 1 - lower_tmatrix !! 1 !! 0 * z1
        z3 = matrix_Y !! 2 - lower_tmatrix !! 2 !! 0 * z1 - lower_tmatrix !! 2 !! 1 * z2 

    in [z1, z2, z3]



main :: IO()
main = do
    let matrix_L = get_matrix_L_and_U matrix !! 0
        matrix_U = get_matrix_L_and_U matrix !! 1
        matrix_Z = get_matrix_Z matrix_L matrix_Y
        x3 = matrix_Z !! 2 / matrix_U !! 2 !! 2
        x2 = (matrix_Z !! 1 - matrix_U !! 1 !! 2 * x3) / matrix_U !! 1 !! 1
        x1 = (matrix_Z !! 0 - matrix_U !! 0 !! 1 * x2 - matrix_U !! 0 !! 2 * x3) / matrix_U !! 0 !! 0

    putStrLn "L:"
    print_matrix matrix_L 0

    putStrLn "\nU:"
    print_matrix matrix_U 0

    putStrLn $ "\nx1 : " ++ show x1 ++ "\nx2 : " ++ show x2 ++ "\nx3 : " ++ show x3 ++ "\n"

