-- slowest method


f :: Double -> Double
f x = cos x - x


bisection :: (Double -> Double) -> Double -> Double -> Double -> Int -> IO()
bisection f a b tol i = do

    if f a * f b > 0
        then 
            error (" [!]  There is no solution between " ++ show a ++ " and " ++  show b )
        else
            let x = (a + b) / 2
                result = f x

            in if  abs (a - b) < tol   -- stop condition
                    then putStrLn $ "x ≈ " ++  show x 
                    else do
                    
                        putStrLn $ replicate (2 - length (show i)) '0' ++ show i ++ ". iteration:" ++ replicate 4 ' '  ++ 
                            "a = " ++ show a ++ replicate (26 - length (show a)) ' ' ++
                            "b = " ++ show b ++ replicate (26 - length (show b)) ' ' ++ 
                            "f(x) = " ++ show result ++ replicate (27 - length (show result)) ' ' ++
                            "|a-b| = " ++ show (abs (a - b))


                        if result == 0
                            then putStrLn $ "x = " ++ show x
                            else if f a * result > 0         -- f(a) and f(x) have same sign
                                then bisection f x b tol (i+1)
                                else bisection f a x tol (i+1)



-- Test function
main :: IO ()
main = do
    let a = 0           -- left
        b = pi/2        -- right
        tol = 10**(-6)  -- Tolerans. for exect solution tol=0

  
    bisection f a b tol 1

  