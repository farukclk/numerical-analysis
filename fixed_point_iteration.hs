{-
    - Test Functions
        i) has_any_root
        ii) has_unique_root
    
-}

{-# LANGUAGE MultiWayIf #-}

g :: Double -> Double
g x = 3**(-x) 


g' :: Double -> Double
g' x =  - 3**(-x) * log 3




-- check g(x) ∈ [a, b] for all x ∈ [a, b]
-- then g has at least one fixedpoint in [a, b].
has_any_root_in_interval :: (Double -> Double) -> Double -> Double -> Bool
has_any_root_in_interval g a b =  all (\y -> a <= y && y <= b)   [g x | x <- takeWhile (<= b) [a, a + 0.00001 .. ] ]  




-- check g'(x) < 1 for all x ∈ [a, b] 
-- Of course, after making sure that at least one root exists 
-- returning True means there is exactly one root in the interval, otherwise does not ensure a unique fixed point 
has_unique_root_in_interval :: (Double -> Double) -> Double -> Double -> String
has_unique_root_in_interval g' a b = do
    let key = all (\y -> (abs y) < 1)  [g' x | x <- takeWhile (<= b) [a, a + 0.00001 ..] ]
    if key == True
        then "True"
        else "does not ensure"
    



fixed_point_iteration :: (Double -> Double) -> Double -> Double -> Int -> IO()
fixed_point_iteration g x0 tol i = do
    let x1 = g x0
        diff = abs (x1 - x0)
    putStrLn $ replicate (2 - length (show i)) '0' ++ show i ++ ". iteration:" ++ replicate 4 ' '  ++ 
        "x1 = " ++ show x1 ++ replicate (26 - length (show x1)) ' ' ++
        "|x1-x0| = " ++ show diff
    

    if | x1 == x0 -> putStrLn $ "x = " ++ show x1
        | diff < tol -> putStrLn $ "x ≈ " ++ show x1
        | diff > tol -> fixed_point_iteration g x1 tol (i+1)






main :: IO()
main = do
    let a = 0.0
        b = 1.0
        x0 = 0.5
        tol = 0.001



    let has_any_root = has_any_root_in_interval g a b

    putStrLn $ "g(x) has at least one fixed point in [" ++ show a ++ ", " ++ show  b ++ "] : " ++ show has_any_root

    if | has_any_root -> do
            putStrLn ""     
            putStrLn $ "g(x) has unique fixed point in ["++ show a ++ ", " ++ show  b ++ "] : " ++ has_unique_root_in_interval g' a b 
            putStrLn "--------------------iterations-----------------------"
            fixed_point_iteration g x0 tol 1
        | otherwise -> putStrLn ""

    



