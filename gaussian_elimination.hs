{-# LANGUAGE MultiWayIf #-}
import Data.List (intercalate)
import Text.Printf (printf)


{-

step 1: convert given matrix to unit upper triangular matrix

step 2: evaluate x values starting from last row

step 3: dump x values to console

-}

-- matrix format: 
-- ax + by + cz = Y  ==>  [a, b, c, Y]

matrix :: [[Double]]
matrix = [[-4, 8, 1, -2, 16]
        , [3, -2, 9, 2, -3]
        , [7, 2, -3, 1, -1]
        , [1, -1, 1, 4,  5]
        ]



-- [a..]  /=a  =>   [1..]
pivot_row :: [[Double]] -> Int -> [[Double]]
pivot_row matrix pivot = 
    let pivot_value = (matrix !! pivot) !! pivot
        new_row = map (/pivot_value) (matrix !! pivot)
    in (take pivot matrix) ++ [new_row] ++ (drop (pivot+1) matrix)



-- 0 <= pivot <= (length-1)
--  make 0 all numbers below pivot
gaussian_elimination :: [[Double]] -> [[Double]] -> Int -> Int -> [[Double]]
gaussian_elimination _matrix matrix2 pivot iter = do
    let matrix =  pivot_row _matrix 0
    if | pivot == length matrix -> pivot_row matrix (pivot-1)
       | iter <= pivot -> do
            let matrix3 = take (pivot +1) matrix 
            gaussian_elimination matrix matrix3 pivot (pivot+1)
       
        | iter == length matrix -> do     
            let matrix3 = pivot_row matrix2 1
            gaussian_elimination matrix3 [] (pivot+1) 0 
              
        | otherwise -> 
            let iter_value = (matrix !! iter) !! pivot
                new_row = zipWith (-) (matrix !! iter) (map (*iter_value) (matrix !! (pivot)))

            in gaussian_elimination matrix (matrix2 ++ [new_row]) pivot (iter+1)
          


-- iter = length matrix
get_x_values :: [[Double]] -> [Double] -> Int -> [Double]
get_x_values matrix x_list iter 
    | iter == 0 = x_list
    | otherwise = get_x_values matrix (x:x_list) (iter-1) where
        row = matrix !! (iter-1)
        y = last row
        x = y - sum (zipWith (*) x_list (drop iter (init row)))
     



print_matrix :: [[Double]] -> Int -> IO()
print_matrix matrix row = do
    if length matrix == row 
        then return()
        else do
            let formatted_elements = map (printf "%-15.6f") (matrix !! row)
                formatted_string = intercalate " " formatted_elements

            putStrLn formatted_string   
            print_matrix matrix (row+1)



-- iter = 0
print_x_values :: [Double] -> Int -> IO() 
print_x_values x_list iter = do
    if iter == length x_list
        then return()
        else do
            putStrLn $ "x"++ show (iter+1) ++ ": " ++ show (x_list !! iter)
            print_x_values x_list (iter+1)



-- Test function 
main :: IO ()
main = do
    let unit_upper_triangular_matrix = gaussian_elimination matrix [] 0 0
        x_list = get_x_values unit_upper_triangular_matrix [] (length unit_upper_triangular_matrix)

    print_matrix unit_upper_triangular_matrix 0
    putStrLn "--------------"
    print_x_values x_list 0


  

