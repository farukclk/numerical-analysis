{-# LANGUAGE MultiWayIf #-}
import Data.List (intercalate)
import Text.Printf (printf)


-- augmented matrix
-- [a, b, c]  => ax + by = cz

matrix :: [[Double]]
matrix = [ [5, 2, 1, 12]
         , [2, 3, 4, 20]
         , [-4, 5, 3, 15]
         ]

x_list :: [Double]
x_list = [0, 0, 0]


iteration_limit :: Int
iteration_limit = 5

--------------------------------------------------


-- herbir satırda mutlak değerce en buyuk olan sayı kosegene gelicek sekilde satırları yer değiştir
-- 0. satırdan ilerle
-- n.stairdaki en buyuk eleman n. sutunda ise bitir
-- aksi durumda n.starıdaki en buyuk eleman m.stunda ise m. satır ile n.staiti yer değiştir
prepare_matrix :: [[Double]] -> Int -> [[Double]]
prepare_matrix matrix row = do
    if | row == length matrix ->  matrix
        | otherwise -> do
            let index = get_index_of_bigest_num_in_row (matrix !! row) 0 0 -- satirdaki en buyuk sayının indexi
            
            if | index  > row ->  prepare_matrix (swap_rows matrix row index) row  -- index ile row satirlarini yer değiştir ve tekrar çalıştır
                | otherwise -> prepare_matrix matrix (row+1)  -- bir alt satıra geç
                    
            
           

get_index_of_bigest_num_in_row :: [Double] -> Int -> Int -> Int
get_index_of_bigest_num_in_row row ni bi 
    | length row - 1 == ni = bi     
    | abs (row !! ni) > abs (row !! bi) = get_index_of_bigest_num_in_row row (ni+1) ni 
    | otherwise = get_index_of_bigest_num_in_row row (ni+1) bi 




--print matrix
print_matrix :: [[Double]] -> Int -> IO()
print_matrix matrix row = do
    if length matrix == row 
        then return()
        else do
            putStrLn (intercalate "    " (map show (matrix !! row))) -- (unwords (map show (matrix !! row)))
            print_matrix matrix (row+1)


print_x_values :: [Double] -> Int -> IO()
print_x_values x_list k =  putStrLn $ "k:" ++ show k ++ "|   " ++  formatted_string where
    formatted_elements = map (printf "%-20.10f") x_list
    formatted_string = intercalate " " formatted_elements


swap_rows :: [a] -> Int -> Int -> [a] 
swap_rows [] _ _ = []
swap_rows xs i j 
    | i < 0 || j < 0 || i >= length xs || j >= length xs = xs
    | otherwise = [if k == i then xs !! j else if k == j then xs !! i else xs !! k | k <- [0..length xs - 1]]



multiply_and_sum :: Num a => [a] -> [a] -> a
multiply_and_sum xs ys = sum (zipWith (*) xs ys)


--iteration
jocabi :: [[Double]] -> [Double] -> Int -> IO()
jocabi matrix x_list limit = jocabi' matrix x_list x_list limit 1 0

jocabi' :: [[Double]] -> [Double] -> [Double] -> Int -> Int -> Int -> IO()
jocabi' matrix x_list x_list2 limit n i = do

   
    let row = matrix !! (n-1)
        x_value = (((last row) - (multiply_and_sum row x_list) + row !! (n-1) * x_list !! (n-1)) / row !! (n-1)) 
        new_x_list = take (n-1) x_list2 ++ x_value:(drop n x_list2)


    
  
    if | limit == i && n == length row - 1 ->  print_x_values new_x_list i     -- son iterasyon ve son xn degeri
        | n == length row - 1 -> do      -- son xn değeri, bir sonraki iterastona gec
            print_x_values new_x_list i
            jocabi' matrix new_x_list new_x_list limit 1 (i+1)  
        | otherwise ->  jocabi' matrix x_list new_x_list limit (n+1)  i  -- bir sonraki xn degerini hesapla



main :: IO()
main = do

    if length x_list /= length matrix 
        then error("Check matrix and x")
        else do

            putStrLn ""
            putStrLn "Augmented Matrix: "
            print_matrix matrix 0
            putStrLn (replicate 20 '-')
            putStrLn "" 
        

            putStrLn  "pivoting: " 
           
            let new_matrix = prepare_matrix matrix 0
               
            print_matrix new_matrix 0
            putStrLn (replicate 20 '-')
            putStrLn ""


            putStrLn $ "             x" ++ (intercalate "                   x" (map show [1..(length matrix)])) 
            jocabi new_matrix x_list iteration_limit 



