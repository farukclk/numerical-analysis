{-
    - Newton’s (or the Newton-Raphson) method is one of the most powerful and well-known
numerical methods for solving a root-finding problem.


    Newton-Raphson Formula:

    x1 = x0 - f(x0) / f'(x0)

    ----

    f(x) = 0
    f(x) = f(x0) + f'(x0)(x - x0) + F(z)

    x = x0 - f(x) / f'(x0) 


-}

f :: Double -> Double
f x = cos x - x


f' :: Double -> Double
f' x = - sin x - 1

newton_raphson :: (Double -> Double) -> (Double -> Double) -> Double -> Double -> Double -> Double -> Int -> IO()
newton_raphson f f' a b x0 tol i = do
    if f a * f b > 0
        then 
            error (" [!]  There is no solution between " ++ show a ++ " and " ++  show b )
        else
            if f' x0 == 0       -- check if f'(x) == 0
                then error (" [!] f'(x) is 0")
                else do
                    let x1 = x0 - f x0 / f' x0
                        result = f x1 
                        difference = abs (f x0 - result)


                    putStrLn $ replicate (2 - length (show i)) '0' ++ show i ++ ". iteration:" ++ replicate 4 ' ' ++
                        "|f(x0)-f(x1)| = " ++ show difference ++ replicate (30 - length (show difference)) ' ' ++
                        "f(x1) = " ++ show result ++ replicate (30 - length(show result)) ' ' ++
                        "x1 = " ++  show x1

                    -- stop condition
                    if result == 0
                        then putStrLn $ "x = " ++ show x1 
                    else if difference > tol
                        then newton_raphson f f' a b x1 tol (i+1)
                        else putStrLn $ "x ≈ " ++ show x1 
                        




-- Test function
main :: IO()
main = do
    let a = 0                -- left
        b = (pi/2)           -- right
        x0 = (pi/4)
        tol = 10**(-6)       -- Tolerans. for exect solution tol=0s
    
    newton_raphson f f' a b x0 tol 1

