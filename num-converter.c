/*
gcc num-converter.c -lm
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>


void digertabanlaribul(int sayi, int taban) { //onluk bir sayi
    int sayi2= sayi;
    int *dizi, e_saysisi=0;
    dizi = (int*)malloc(sizeof(int) * 1);
    int i=0; 
    int ust = 0;
    if (sayi < taban) {
        dizi[0] = sayi; 
        e_saysisi=1;
    }
    else {
        while (sayi >= taban) {
            dizi[i] = (sayi % taban);
            sayi = sayi/taban;
            i++;
            e_saysisi++;
        
        }
            
        dizi[i] = sayi; 
        e_saysisi++;
    }

    printf("\nGirilen sayı: %d\n", sayi2);
    printf("%d tabana dönüştürlmüş sayı: ",taban);
    for (i=e_saysisi-1; i >=0; i--) {
        if (dizi[i] == 10)
             printf("%c",'A');
        else if (dizi[i] == 11)
             printf("%c",'b');
        else if (dizi[i] == 12)
             printf("%c",'C');
        else if (dizi[i] == 13)
             printf("%c",'D');
        else if (dizi[i] == 14)
             printf("%c",'E');
        else if (dizi[i] == 15) 
             printf("%c",'F');
        else
             printf("%d", dizi[i]);
       
    }
    printf("\n");
    free(dizi);
    
}

void onlukbul(char dizi[], int taban) {
    int e_sayisi=0;
    while (dizi[e_sayisi] !=0 ){
        e_sayisi++;
    }
    char tmp;
    int toplam = 0, ust=0;
    while (e_sayisi > 0) {
        tmp = dizi[e_sayisi-1];
   
        if (tmp == 'A' || tmp== 'a')
             toplam += pow(taban, ust) * 10;
        else if (tmp == 'B' || tmp== 'b')
             toplam += pow(taban, ust) * 11;
        else if (tmp == 'C' || tmp== 'c')
             toplam += pow(taban, ust) * 12;
        else if (tmp == 'D' || tmp== 'd')
             toplam += pow(taban, ust) * 13;
        else if (tmp == 'E' || tmp== 'e')
             toplam += pow(taban, ust) * 14;
        else if (tmp == 'F' || tmp== 'f') 
             toplam += pow(taban, ust) * 15;
        else
            toplam += pow(taban, ust) * (tmp - '0');

        e_sayisi--;
        ust++;
    }
    printf("\n%d tabanda girilen sayı: %s\n", taban, dizi);
    printf("10 luk tabana dönüştürlmüş sayı: %d\n",toplam);
   
}

int main () {
    int secim, taban, key=1;
   while (1) {
        system("clear");
        printf("1- 10’luk tabandan çevir\n");
        printf("2- 10’luk tabana çevir\n");
        printf("3- Çıkış\n");
        printf("\nLütfen seçiminizi yapınız: ");
        scanf("%d", &secim);
        system("clear");
        if (secim == 1) {
            int sayi;
            printf("sayı: ");
            scanf("%d", &sayi);
            printf("taban: ");
            scanf("%d", &taban); 
            digertabanlaribul(sayi, taban);
        }
        else if (secim == 2) {
            char dizi[100]; 
            printf("sayı: ");
            scanf("%s",&dizi);
            printf("taban: ");
            scanf("%d", &taban);
            onlukbul(dizi, taban);
            
        }
        else if (secim == 3) {
            exit(0);
        } 
        printf("\n[ent]");
        secim = getchar();
        secim = getchar();
    }
    return 0;
}
