{-
    False Positon or Regula Falsi Method

    formula : 
        x = ( a * f(b) - b * f(a)) ) / ( f(b) - f(a) )

    -----
    newton's method formula = "x1 = x0 - f(x0) / f'(x0)" and it is a problem when we do not know what f'(x) is. To solve that we can say 
    f'(x) = (f(b) - f(a)) / (b - a)  and assume x0 = a so we obtain the formula above.

    - faster than bisection method
    - slower than newton's method


-}

{-# LANGUAGE MultiWayIf #-}

f :: Double -> Double
f x = cos x - x


regula_falsi :: Double -> Double -> Double -> Int -> IO()
regula_falsi a b tol i = do 
    if (f a * f b) > 0
        then error (" [!]  There is no solution between " ++ show a ++ " and " ++  show b )
        else do
            let x1 = (a * f b - b * f a) / (f b - f a)
                stop_condition = max (abs b-x1) (abs x1-a)  -- you can edit stop-condition type
                result = f x1 

            putStrLn $ replicate (2 - length (show i)) '0' ++ show i ++ ". iteration:" ++ replicate 4  ' ' ++
                "a = " ++ show a ++ replicate (26 - length(show a)) ' ' ++
                "b = " ++ show b ++ replicate (26 - length(show b)) ' ' ++
                "x = " ++ show x1 ++ replicate (30 - length(show x1))  ' ' ++
                "f(x) = " ++ show result

            if | result == 0 -> putStrLn $ "x = " ++ show x1            -- check f(x) is 0
                | stop_condition < tol -> putStrLn $ "x ≈ " ++ show x1  -- check stop condition
                | f a *  result > 0 -> regula_falsi x1 b tol (i+1)      -- f(a) and f(x1) have same sign 
                | otherwise -> regula_falsi a x1 tol (i+1)              -- f(a) and f(x1) have opposite sign 
                    


-- Test function
main :: IO()
main = do
    let a = 0           -- left
        b = pi/2        -- right
        tol = 10**(-6)  -- Tolerans. for exect solution tol=0
    
    regula_falsi a b tol 1
