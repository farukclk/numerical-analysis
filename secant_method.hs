{-

    Newton-Raphson Formula:

        x1 = x0 - f(x0) / f'(x0)
        x2 = x1 - f(x1) / f'(x1)
                :
                :


                f(x1) - f(x0)  
    f'(x1) =   ---------------
                   x1 - x0 

                
    Secant Method Formula:

                                x1 - x0
    x2 = x1  -  f(x1)  *   -------------------
                             f(x1) - f(x(0))

                            
    - find x2 using given x1 and x0, then repeat this expression

-}


f :: Double -> Double
f x  =  cos x  - x

secant_method :: (Double -> Double) -> Double -> Double -> Double -> Int -> IO()
secant_method f x0 x1 tol i = do
    let x2 = x1 - f x1 * (x1 - x0) / (f x1 - f x0)
        result = f x2

    putStrLn $ replicate (2 - length (show i)) '0' ++ show i ++ ". iteration:" ++ replicate 4 ' ' ++ 
        "f(x) = " ++ show result ++ replicate (30 - length (show result)) ' ' ++
        "x = " ++ show x2

    if result == 0
        then putStrLn $ "x = " ++  show x2
        else if abs (x2 -x1) > tol
            then secant_method f x1 x2 tol (i+1)
            else putStrLn $ "x  ≈ " ++  show x2


     
     
main :: IO()
main = secant_method f 0 (pi/2) (10**(-6)) 1
